# frozen_string_literal: true

require './lib/substractor'

describe Substractor do
  let(:payload) do
    File.open("./spec/fixtures/#{filename}.json").read
  end

  subject { Substractor.new(payload) }
  let(:result) { subject.call }

  context 'not json' do
    let(:filename) { 'not_json' }

    it 'validates' do
      expect(result).to eq(error: 'Bad Input')
    end
  end

  context 'wrong format' do
    let(:filename) { 'wrong_format' }

    it 'validates' do
      expect(result).to eq(error: 'Wrong format')
    end
  end

  context 'wrong data types' do
    let(:filename) { 'wrong_data_types' }

    it 'validates' do
      expect(result).to eq(error: 'Wrong data types')
    end
  end

  context 'wrong pair size' do
    let(:filename) { 'wrong_pair_size' }

    it 'validates' do
      expect(result).to eq(error: 'Wrong values size')
    end
  end

  context 'valid' do
    let(:filename) { 'valid' }

    it 'ok' do
      expect(result).to eq [-6, 0, 4, 3, -7, 5]
    end
  end
end
