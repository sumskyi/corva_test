# frozen_string_literal: true

require 'bundler'
Bundler.require
require 'rack/test'
require './corva'

describe Corva::Application do
  include Rack::Test::Methods

  def app
    Corva::Application
  end

  let(:body) { JSON.parse(last_response.body) }
  let(:success) { { 'status' => 'ok' } }

  let(:timestamp) { 1_493_758_596 }
  let(:response) do
    {
      'request_id' => 234_567,
      'timestamp' => timestamp,
      'result' => {
        'title' => 'Result',
        'values' => result
      }
    }
  end

  it 'heartbeat' do
    get '/'
    expect(body).to eq success
  end

  let(:payload) do
    File.open("./spec/fixtures/#{filename}.json").read
  end

  context 'wrong data type' do
    let(:filename) { 'wrong_data_types' }
    let(:result) { { 'error' => 'Wrong data types' } }

    it 'error on wrong data type' do
      post '/compute/234567', payload: payload
      expect(body).to eq(response)
    end
  end

  context 'wrong format' do
    let(:filename) { 'wrong_format' }
    let(:result) { { 'error' => 'Wrong format' } }
    let(:timestamp) { nil }

    it 'error on wrong format' do
      post '/compute/234567', payload: payload
      expect(body).to eq(response)
    end
  end

  context 'wrong pair size' do
    let(:filename) { 'wrong_pair_size' }
    let(:result) { { 'error' => 'Wrong values size' } }

    it 'error on wrong format' do
      post '/compute/234567', payload: payload
      expect(body).to eq(response)
    end
  end

  context 'success' do
    let(:filename) { 'valid' }
    let(:result) { [-6, 0, 4, 3, -7, 5] }

    it 'error on wrong format' do
      post '/compute/234567', payload: payload
      expect(body).to eq(response)
    end
  end
end
