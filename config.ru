# frozen_string_literal: true

require 'rubygems'
require 'bundler'

Bundler.require

require File.expand_path('corva', File.dirname(__FILE__))

run Corva::Application
