# Test task for Corva

## Programming Problem
Your task is to build a POST web service that accepts a JSON value, computes and returns a result. Deliver the final endpoint for testing.

Endpoint: `http://XXX/compute/<request_id>`

### Input POST Value:
```json
{
  “timestamp”: 1493758596,
  “data”: [
    {
      “title”: “Part1”,
      “values”: [0, 3, 5, 6, 2, 9]
    },
    {
      “title”: “Part2”,
      “values”: [6, 3, 1, 3, 9, 4]
    }
  ]
}
```

The service shall take the array from Part 1 and subtract the values from Part 2, subtracting numbers that exist in the same index of the array. The final array is also the same size (6).

The return value will be a JSON document in the following format containing the resultant array and request ID.

### Output:
```json
{
  “request_id”: “<request_id>”,
  “timestamp”: 1493758596,
  “result”: {
    “title”: “Result”,
    “values”: […]
  }
}
```

### To run tests
```sh
bundle exec rspec
```

### cURL commands for testing
- **wrong data type:** `curl -X POST  -H "Content-Type: application/json" https://corva88.herokuapp.com/compute/234567 -d "{\"payload\":{\"timestamp\":1493758596,\"data\":[{\"title\":\"Part 1\",\"values\":[0,3,5,6,2,\"Y\"]},{\"title\":\"Part 2\",\"values\":[6,3,1,3,9,4]}]}}"`
- **wrong format:** `curl -X POST  -H "Content-Type: application/json" -d "{\"payload\":{\"stamp\":1493758596,\"payload\":[{\"title\":\"Part 1\",\"values\":[0,3,5,6,2,9]},{\"title\":\"Part 2\",\"values\":[6,3,1,3,9,4]}]}}" https://corva88.herokuapp.com/compute/234567`
- **wrong pair size:** `curl -X POST  -H "Content-Type: application/json" -d "{\"payload\":{\"timestamp\":1493758596,\"data\":[{\"title\":\"Part 1\",\"values\":[0,3,5,6,2]},{\"title\":\"Part 2\",\"values\":[6,3,1,3,9,3]}]}}"  https://corva88.herokuapp.com/compute/234567`
- **success:** `curl -X POST  -H "Content-Type: application/json" -d "{\"payload\":{\"timestamp\":1493758596,\"data\":[{\"title\":\"Part 1\",\"values\":[0,3,5,6,2,9]},{\"title\":\"Part 2\",\"values\":[6,3,1,3,9,4]}]}}" https://corva88.herokuapp.com/compute/234567`
