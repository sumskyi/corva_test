# frozen_string_literal: true

require './lib/substractor'

module Corva
  # Rack Grape application
  class Application < Grape::API
    format :json

    helpers do
      def timestamp(json)
        json.is_a?(String) ? JSON.parse(json)['timestamp'] : json['timestamp']
      rescue JSON::ParserError
        nil
      end
    end

    desc 'heartbeat'
    get '/' do
      { status: 'ok' }
    end

    desc 'Substruction calculation service endpoint'
    params do
      requires :request_id, type: Integer, desc: 'Request id.'
      requires :payload, desc: 'Post data payload'
    end
    post 'compute/:request_id' do
      values = Substractor.new(params[:payload]).call
      {
        request_id: params[:request_id],
        timestamp: timestamp(params[:payload]),
        result: {
          title: 'Result',
          values: values
        }
      }
    end
  end
end
