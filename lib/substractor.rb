# frozen_string_literal: true

require 'json'

# The goal of Substractor is:
# - convert JSON string to Ruby hash
# - validate user data
# - substract values
# - return resulting array
class Substractor
  BadFormat   = Class.new(StandardError)
  BadDataType = Class.new(StandardError)
  BadPairSize = Class.new(StandardError)

  def initialize(payload)
    @json = payload.is_a?(String) ? JSON.parse(payload) : payload
    validate
  rescue JSON::ParserError
    @result = { error: 'Bad Input' }
  rescue BadFormat, BadDataType, BadPairSize => e
    @result = { error: e.message }
  end

  def call
    return @result if @result

    @json['data'][0]['values'].to_enum.with_index(0).map do |el, i|
      el - @json['data'][1]['values'][i]
    end
  end

  private

  def validate
    validate_format
    validate_pairs
    validate_data_types
  end

  def validate_format
    @json['timestamp'] &&
      @json['data'] &&
      @json['data'].size == 2 &&
      @json['data'].is_a?(Array) &&
      @json['data'].all? { |d| d['values'].is_a?(Array) } ||
      (raise BadFormat, 'Wrong format')
  end

  def validate_pairs
    @json['data'][0]['values'].size == @json['data'][1]['values'].size ||
      (raise BadPairSize, 'Wrong values size')
  end

  def validate_data_types
    @json['data'].all? do |d|
      d['values'].all? { |v| v.is_a?(Integer) }
    end || (raise BadDataType, 'Wrong data types')
  end
end
